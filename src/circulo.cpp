#include "circulo.hpp"
#include <iostream>
#include <string>


Circulo::Circulo(string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}
Circulo::~Circulo(){}

float Circulo::calcula_area(){       //sobrescrita
    return ((get_altura()/2)*(get_altura()/2)* 3.14); //area circulo = raio^2*pi
}
float Circulo::calcula_perimetro(){  //sobrescrita
    return (2*3.14*(get_altura()/2)); //perimetro circulo = 2*pi*raio
}