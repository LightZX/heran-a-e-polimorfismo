#include "quadrado.hpp"
#include <iostream>
#include <string>


Quadrado::Quadrado(string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}
Quadrado::~Quadrado(){}

float Quadrado::calcula_area(){ //sobrescrita 
    return (get_altura() * get_altura());
}
float Quadrado::calcula_perimetro(){ //sobrescrita
    return (get_altura() * 4);
}