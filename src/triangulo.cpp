#include "triangulo.hpp"
#include <iostream>
#include <string>


Triangulo::Triangulo(string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}
Triangulo::~Triangulo(){}

float Triangulo::calcula_area(){         //sobrescrita
    return ((get_base() * get_altura())/2);
}
float Triangulo::calcula_perimetro(){    //sobrescrita
    return (get_base()*get_base()*get_base());
}