#include "hexagono.hpp"
#include <iostream>
#include <string>


Hexagono::Hexagono(string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}
Hexagono::~Hexagono(){}

float Hexagono::calcula_area(){       //sobrescrita
    return (6 *((get_base() * (get_altura()/2)) /2)); // (get_altura()/2) usada como apotema
}
float Hexagono::calcula_perimetro(){  //sobrescrita
    return (6 * (get_base()));
}