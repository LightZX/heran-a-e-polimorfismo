#include "formageometrica.hpp"
#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include <string>

using namespace std;

class Circulo: public FormaGeometrica{
private:

public:
    Circulo(string tipo, float base, float altura);
    ~Circulo();
    float calcula_area();
    float calcula_perimetro();
    
};

#endif