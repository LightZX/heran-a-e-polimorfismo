#include "formageometrica.hpp"
#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP

#include <string>

using namespace std;

class Paralelogramo: public FormaGeometrica{
private:

public:
    Paralelogramo(string tipo, float base, float altura);
    ~Paralelogramo();
    float calcula_area();
    float calcula_perimetro();

};

#endif