#include <iostream>
#include "triangulo.hpp"
#include "circulo.hpp"
#include "quadrado.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"

#include <vector>

using namespace std;

int main(){
  float base, altura;
  
  //usuario que escolhe a base e altura
  cout << "Digite a base e a altura desejadas respectivamente para que seja calculado a área e o perímetro das formas geometricas." << endl;
  cout << "Obs: A altura, no circulo é usada como diametro, no pentagono ela é usada como apotema e no quadrado é usada como o lado." << endl;
  cout << endl;
  cin >> base >> altura;
  
  vector <FormaGeometrica*> formas;

	
	formas.push_back(new Triangulo("Triangulo", base, altura));
	formas.push_back(new Circulo("Circulo", base, altura));
  formas.push_back(new Quadrado("Quadrado", base, altura));
	formas.push_back(new Paralelogramo("Paralelogramo", base, altura));
	formas.push_back(new Pentagono("Pentagono", base, altura));
	formas.push_back(new Hexagono("Hexagono", base, altura));

  for(FormaGeometrica *p : formas){
		cout << endl;
    cout << "Tipo: " << p-> get_tipo() << endl;
		cout << "Area: " << p-> calcula_area() << endl;
		cout << "Perimetro: " << p-> calcula_perimetro() << endl;

  }

  cout << endl;

  for(FormaGeometrica *p : formas){
    delete p;

  }

  while(!formas.empty()){
    formas.pop_back();
  }
  
  return 0;  
}